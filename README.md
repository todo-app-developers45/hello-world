# Testing Markdown

## Table of Content

[[_TOC_]]

## Project :tada

This is the Readme file

```shell
npm install
```

### JavaScript calls

A simple JavaScript call to launch the service

```javascript
function launch() {
    //handle the server launch
}

app.start(1234, launch);
```

### Node.js requirements

A Node.js app requires three things:

- [ ] NPM for package management
- [ ] JavaScript for program inteface
- [x] Passion and sometime patience. :wink:

Let me try something else.

Hello.

```mermaid
graph TD
    A[Javascript] -->|Compiles down to| B(V8) 
```
